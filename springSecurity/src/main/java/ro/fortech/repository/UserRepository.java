package ro.fortech.repository;

import java.util.List;

import ro.fortech.model.User;

public interface UserRepository {

	List<User> findAll();
	
}
