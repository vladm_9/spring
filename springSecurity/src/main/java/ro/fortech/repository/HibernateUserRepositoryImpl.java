package ro.fortech.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import ro.fortech.model.User;

@Repository("userRepository")
public class HibernateUserRepositoryImpl implements UserRepository {
	
	public List<User> findAll() {
		List<User> users = new ArrayList<>();
		
		User user = new User();
		user.setFirstName("Vlad");
		user.setLastName("Muresan");
		
		users.add(user);
		return users;
	}

}
 