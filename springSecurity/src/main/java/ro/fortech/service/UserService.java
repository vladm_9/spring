package ro.fortech.service;

import java.util.List;

import ro.fortech.model.User;

public interface UserService {

	public List<User> findAll();
}
