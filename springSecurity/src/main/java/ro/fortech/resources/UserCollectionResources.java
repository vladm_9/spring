package ro.fortech.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import ro.fortech.service.UserService;

@Controller
public class UserCollectionResources {
	
	@Autowired
	UserService userService;
	
	@RequestMapping("/name")
    @ResponseBody
    String getName() {
        return "Name: " + userService.findAll().get(0).getFirstName();
    }

}
