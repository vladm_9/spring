package ro.fortech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ro.fortech.service.UserService;
import ro.fortech.service.UserServiceImpl;

@SpringBootApplication
public class Application {
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		
		ApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		UserService userService = appContext.getBean("userService", UserService.class);
		
		System.out.println(userService.findAll().get(0).getFirstName());
	}
	
}
